<?php

/**
 * Bit&Black IDML Writer. Creating Adobe InDesign Markup Language files.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\IdmlWriter;

use BitAndBlack\IdmlWriter\Exception\FailedCreatingContentException;
use BitAndBlack\IdmlWriter\Exception\WrongContentTypeException;
use BitAndBlack\IdmlWriter\File\Directory;
use BitAndBlack\IdmlWriter\File\File;
use BitAndBlack\IdmlWriter\File\FileInterface;
use BitAndBlack\IdmlWriter\File\FileStack;
use BitAndBlack\IdmlWriter\File\TreeInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use ZipStream\Exception\OverflowException;
use ZipStream\ZipStream;

/**
 * The Writer class takes IDML content and creates an IDML file out of it.
 *
 * @see \BitAndBlack\IdmlWriter\Tests\WriterTest
 */
class Writer
{
    private readonly ZipStream $zipStream;

    /**
     * @var array<string, string|array<string, string>>
     */
    private array $contentOutput = [];

    private static OutputInterface $output;

    /**
     * The directory separator inside ZIP files is always the same, no matter of the operating system.
     */
    private string $zipDirSeparator = '/';
    
    /**
     * @var resource
     */
    private $outputStream;

    /**
     * Writer constructor.
     *
     * @param ContentInterface $contentInput The IDML content object holding all contents.
     * @param TreeInterface $tree The tree object which is responsible for the tree structure.
     * @throws FailedCreatingContentException
     */
    public function __construct(
        private readonly ContentInterface $contentInput,
        private readonly TreeInterface $tree,
    ) {
        $outputStream = fopen('php://memory', 'wb+');

        if (false === $outputStream) {
            throw new FailedCreatingContentException(
                'Not possible to use "php://memory".'
            );
        }

        $this->outputStream = $outputStream;

        $this->zipStream = new ZipStream(
            outputStream: $this->outputStream,
            sendHttpHeaders: false,
        );

        self::$output ??= new NullOutput();

        $this->getIDMLContents();
    }

    /**
     * Enables to have a progress bar in the CLI when creating the IDML file.
     */
    public static function enableProgressBar(OutputInterface $output): void
    {
        self::$output = $output;
    }

    /**
     * Fills the tree with content.
     *
     * __Please note:__ Directories aren't allowed here. IDML files are stored in a flat hierarchy,
     * containing their parent folder names in their filenames.
     *
     * @param string|null $dirName
     * @return void
     * @throws WrongContentTypeException
     */
    private function fillTree(FileInterface $content, string|null $dirName = null): void
    {
        if ($content instanceof File) {
            $fileNameComplete = null !== $content->getSuffix()
                ? $content->getName() . '.' . $content->getSuffix()
                : $content->getName()
            ;
            $subDir = null !== $dirName ? $dirName . $this->zipDirSeparator : null;
            $fileContent = $this->contentOutput[strtolower($content->getName())];

            /** Should never happen because a File may only contain a string */
            if (is_array($fileContent)) {
                throw new WrongContentTypeException(
                    strtolower($content->getName()),
                    'string'
                );
            }

            $this->zipStream->addFile(
                $subDir . $fileNameComplete,
                $fileContent
            );

            return;
        }

        if ($content instanceof Directory) {
            foreach ($content->getContent() as $contentInner) {
                $this->fillTree($contentInner, $content->getName());
            }

            return;
        }

        if ($content instanceof FileStack) {
            $childrenName = $content->getName();
            $suffix = null !== $content->getSuffix()
                ? '.' . $content->getSuffix()
                : null
            ;
            $subDir = null !== $dirName ? $dirName . $this->zipDirSeparator : null;
            $fileStackContent = $this->contentOutput[strtolower($childrenName)];

            /** Should never happen because a FileStack may only contain an array */
            if (!is_array($fileStackContent)) {
                throw new WrongContentTypeException(
                    strtolower($content->getName()),
                    'array'
                );
            }

            foreach ($fileStackContent as $uniqueIdentifier => $contents) {
                $this->zipStream->addFile(
                    $subDir . $childrenName . '_' . $uniqueIdentifier . $suffix,
                    $contents
                );
            }
        }
    }

    /**
     * Creates the IDML file and writes it to the file system.
     *
     * @return bool
     * @throws WrongContentTypeException
     * @throws FailedCreatingContentException
     */
    public function create(string $fileName): bool
    {
        return false !== file_put_contents(
            $fileName,
            $this->getContent()
        );
    }

    /**
     * Creates the IDML file and returns its content.
     *
     * @return string
     * @throws WrongContentTypeException
     * @throws FailedCreatingContentException
     */
    public function getContent(): string
    {
        $this->fillTree($this->tree->getTree());

        try {
            $this->zipStream->finish();
        } catch (OverflowException $overflowException) {
            throw new FailedCreatingContentException(
                'IDML file is too large. Use `setArchiveOptions` to configure the file handling by yourself.',
                0,
                $overflowException
            );
        }

        rewind($this->outputStream);
        $response = stream_get_contents($this->outputStream);
        fclose($this->outputStream);

        return (string) $response;
    }

    /**
     * Creates the XML data for each file.
     *
     * @return void
     */
    private function getIDMLContents(): void
    {
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% %bar% %message%');

        $progressBar = new ProgressBar(self::$output, 12);
        $progressBar->setFormat('custom');
        $progressBar->setMessage('Creating IDML file...');
        $progressBar->start();

        $progressBar->setMessage('Creating Designmap...');
        $this->contentOutput['designmap'] = $this->contentInput->getDesignMapXML();
        $progressBar->advance();

        $progressBar->setMessage('Creating MasterSpreads...');
        $this->contentOutput['masterspread'] = $this->contentInput->getMasterspreadXML();
        $progressBar->advance();

        $progressBar->setMessage('Creating Container...');
        $this->contentOutput['container'] = $this->contentInput->getContainerXML();
        $progressBar->advance();

        $progressBar->setMessage('Creating MetaData...');
        $this->contentOutput['metadata'] = $this->contentInput->getMetaDataXML();
        $progressBar->advance();

        $progressBar->setMessage('Creating Mimetype...');
        $this->contentOutput['mimetype'] = $this->contentInput->getMimeType();
        $progressBar->advance();

        $progressBar->setMessage('Creating Fonts...');
        $this->contentOutput['fonts'] = $this->contentInput->getFontsXML();
        $progressBar->advance();

        $progressBar->setMessage('Creating Graphic...');
        $this->contentOutput['graphic'] = $this->contentInput->getGraphicXML();
        $progressBar->advance();

        $progressBar->setMessage('Creating Preferences...');
        $this->contentOutput['preferences'] = $this->contentInput->getPreferencesXML();
        $progressBar->advance();

        $progressBar->setMessage('Creating Story...');
        $this->contentOutput['story'] = $this->contentInput->getStoryXML();
        $progressBar->advance();

        $progressBar->setMessage('Creating Styles...');
        $this->contentOutput['styles'] = $this->contentInput->getStylesXML();
        $progressBar->advance();

        $progressBar->setMessage('Creating Spreads...');
        $this->contentOutput['spread'] = $this->contentInput->getSpreadXML();
        $progressBar->advance();

        $progressBar->setMessage('Creating Backingstory...');
        $this->contentOutput['backingstory'] = $this->contentInput->getBackingStoryXML();
        $progressBar->advance();

        $progressBar->setMessage('Creating Tags...');
        $this->contentOutput['tags'] = $this->contentInput->getTagsXML();
        $progressBar->finish();

        self::$output->writeln('');
    }
}
