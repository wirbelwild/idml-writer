<?php

/**
 * Bit&Black IDML Writer. Creating Adobe InDesign Markup Language files.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\IdmlWriter\File;

/**
 * The Tree class holds the definition of the file structure and folder structure inside the IDML file.
 */
readonly class Tree implements TreeInterface
{
    private Directory $tree;

    /**
     * Tree constructor.
     *
     * @param Directory|null $tree
     */
    public function __construct(Directory|null $tree = null)
    {
        $this->tree = $tree ??
            new Directory(
                null,
                [
                    new File('designmap', 'xml'),
                    new Directory(
                        'MasterSpreads',
                        [
                            new FileStack('MasterSpread', 'xml'),
                        ],
                    ),
                    new Directory(
                        'META-INF',
                        [
                            new File('container', 'xml'),
                            new File('metadata', 'xml'),
                        ],
                    ),
                    new File('mimetype'),
                    new Directory(
                        'Resources',
                        [
                            new File('Fonts', 'xml'),
                            new File('Graphic', 'xml'),
                            new File('Preferences', 'xml'),
                            new File('Styles', 'xml'),
                        ],
                    ),
                    new Directory(
                        'Spreads',
                        [
                            new FileStack('Spread', 'xml'),
                        ],
                    ),
                    new Directory(
                        'Stories',
                        [
                            new FileStack('Story', 'xml'),
                        ],
                    ),
                    new Directory(
                        'XML',
                        [
                            new File('BackingStory', 'xml'),
                            new File('Tags', 'xml'),
                        ],
                    ),
                ],
            )
        ;
    }

    /**
     * @return Directory
     */
    public function getTree(): Directory
    {
        return $this->tree;
    }
}
