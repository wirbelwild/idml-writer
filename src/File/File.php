<?php

/**
 * Bit&Black IDML Writer. Creating Adobe InDesign Markup Language files.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\IdmlWriter\File;

readonly class File implements FileInterface
{
    /**
     * XMLFile constructor.
     *
     * @param string $name
     * @param string|null $suffix
     */
    public function __construct(
        private string $name,
        private ?string $suffix = null,
    ) {
    }

    /**
     * @return string|null
     */
    public function getSuffix(): ?string
    {
        return $this->suffix;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
