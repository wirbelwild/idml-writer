<?php

/**
 * Bit&Black IDML Writer. Creating Adobe InDesign Markup Language files.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\IdmlWriter\File;

readonly class Directory implements FileInterface
{
    /**
     * Directory constructor.
     *
     * @param string|null $name
     * @param array<int, FileInterface> $content
     */
    public function __construct(
        private ?string $name = null,
        private array $content = [],
    ) {
    }

    /**
     * @return string|null
     */
    public function getSuffix(): ?string
    {
        return null;
    }

    /**
     * @return array<int, FileInterface>
     */
    public function getContent(): array
    {
        return $this->content;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
}
