<?php

/**
 * Bit&Black IDML Writer. Creating Adobe InDesign Markup Language files.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\IdmlWriter\Exception;

use BitAndBlack\IdmlWriter\Exception;
use Throwable;

class FailedCreatingContentException extends Exception
{
    /**
     * FailedCreatingContentException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message, int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct('Failed creating IDML content. ' . $message, $code, $previous);
    }
}
