<?php

/**
 * Bit&Black IDML Writer. Creating Adobe InDesign Markup Language files.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\IdmlWriter\Exception;

use BitAndBlack\IdmlWriter\Exception;
use Throwable;

class WrongContentTypeException extends Exception
{
    /**
     * WrongContentTypeException constructor.
     *
     * @param string $contentName
     * @param string $contentType
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $contentName, string $contentType, int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct('Content "' . $contentName . '" must contain ' . $contentType . '.', $code, $previous);
    }
}
