<?php

/**
 * Bit&Black IDML Writer. Creating Adobe InDesign Markup Language files.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\IdmlWriter;

/**
 * When creating an IDML, the Writer will access these methods to get the needed content.
 */
interface ContentInterface
{
    /**
     * A string with the content for the DesignMap file.
     * Should start with `<?xml`
     *
     * @return string
     */
    public function getDesignMapXML(): string;

    /**
     * An array of all MasterSpreads an their content.
     * Key is the file name and value is the content of that file. For example:
     *
     * `[
     *      'MyMasterSpread1' => '<?xml ... ',
     *      'MyMasterSpread2' => '<?xml ... ',
     * ]`

     * @return array<string, string>
     */
    public function getMasterSpreadXML(): array;

    /**
     * A string with the content for the Container file.
     * Should start with `<?xml`
     *
     * @return string
     */
    public function getContainerXML(): string;

    /**
     * A string with the content for the MetaData file.
     * Should start with `<?xml`
     *
     * @return string
     */
    public function getMetaDataXML(): string;

    /**
     * A string with the content for the MimeType file.
     *
     * @return string
     */
    public function getMimeType(): string;

    /**
     * A string with the content for the Fonts file.
     * Should start with `<?xml`
     *
     * @return string
     */
    public function getFontsXML(): string;

    /**
     * A string with the content for the Graphic file.
     * Should start with `<?xml`
     *
     * @return string
     */
    public function getGraphicXML(): string;

    /**
     * A string with the content for the Preferences file.
     * Should start with `<?xml`
     *
     * @return string
     */
    public function getPreferencesXML(): string;

    /**
     * A string with the content for the Styles file.
     * Should start with `<?xml`
     *
     * @return string
     */
    public function getStylesXML(): string;

    /**
     * An array of all Spreads an their content.
     * Key is the file name and value is the content of that file. For example:
     *
     * `[
     *      'MySpread1' => '<?xml ... ',
     *      'MySpread2' => '<?xml ... ',
     * ]`
     *
     * @return array<string, string>
     */
    public function getSpreadXML(): array;

    /**
     * An array of all Stories an their content.
     * Key is the file name and value is the content of that file. For example:
     *
     * `[
     *      'MyStory1' => '<?xml ... ',
     *      'MyStory2' => '<?xml ... ',
     * ]`
     *
     * @return array<string, string>
     */
    public function getStoryXML(): array;

    /**
     * A string with the content for the BackingStory file.
     * Should start with `<?xml`
     *
     * @return string
     */
    public function getBackingStoryXML(): string;

    /**
     * A string with the content for the Tags file.
     * Should start with `<?xml`
     *
     * @return string
     */
    public function getTagsXML(): string;
}
