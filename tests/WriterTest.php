<?php

/**
 * Bit&Black IDML Writer. Creating Adobe InDesign Markup Language files.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\IdmlWriter\Tests;

use BitAndBlack\Helpers\FileSystemHelper;
use BitAndBlack\IdmlWriter\Exception\FailedCreatingContentException;
use BitAndBlack\IdmlWriter\Exception\WrongContentTypeException;
use BitAndBlack\IdmlWriter\File\Tree;
use BitAndBlack\IdmlWriter\Writer;
use Generator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use ZipArchive;

class WriterTest extends TestCase
{
    private static Writer $writer;

    private static string $tempFile;

    private static string $tempFolder;

    /**
     * Creates an IDML at first
     *
     * @throws FailedCreatingContentException
     * @throws WrongContentTypeException
     */
    public static function setUpBeforeClass(): void
    {
        self::$tempFile = __DIR__ . DIRECTORY_SEPARATOR . 'tempFile.idml';
        self::$tempFolder = __DIR__ . DIRECTORY_SEPARATOR . 'tempFolder';

        self::$writer = new Writer(
            new TempContent(),
            new Tree()
        );

        $success = self::$writer->create(self::$tempFile);

        self::assertTrue($success);

        $zip = new ZipArchive();

        if ($zip->open(self::$tempFile)) {
            $zip->extractTo(self::$tempFolder);
            $zip->close();
        }
    }

    /**
     * Removes the IDML and its folder.
     */
    public static function tearDownAfterClass(): void
    {
        unlink(self::$tempFile);
        FileSystemHelper::deleteFolder(self::$tempFolder);
    }

    public static function getCanWriteContentData(): Generator
    {
        yield [
            'MasterSpreads' . DIRECTORY_SEPARATOR . 'MasterSpread_SPREAD1.xml',
            'MASTERSPREAD',
        ];

        yield [
            'META-INF' . DIRECTORY_SEPARATOR . 'container.xml',
            'CONTAINER',
        ];

        yield [
            'META-INF' . DIRECTORY_SEPARATOR . 'metadata.xml',
            'METADATA',
        ];

        yield [
            'Resources' . DIRECTORY_SEPARATOR . 'Fonts.xml',
            'FONTS',
        ];

        yield [
            'Resources' . DIRECTORY_SEPARATOR . 'Graphic.xml',
            'GRAPHIC',
        ];

        yield [
            'Resources' . DIRECTORY_SEPARATOR . 'Preferences.xml',
            'PREFERENCES',
        ];

        yield [
            'Resources' . DIRECTORY_SEPARATOR . 'Styles.xml',
            'STYLES',
        ];

        yield [
            'Spreads' . DIRECTORY_SEPARATOR . 'Spread_SPREAD1.xml',
            'SPREAD',
        ];

        yield [
            'Stories' . DIRECTORY_SEPARATOR . 'Story_STORY1.xml',
            'STORY',
        ];

        yield [
            'XML' . DIRECTORY_SEPARATOR . 'BackingStory.xml',
            'BACKINGSTORY',
        ];

        yield [
            'XML' . DIRECTORY_SEPARATOR . 'Tags.xml',
            'TAGS',
        ];

        yield [
            'designmap.xml',
            'DESIGNMAP',
        ];

        yield [
            'mimetype',
            'MIMETYPE',
        ];
    }

    /**
     * Tests if an IDML file can be created
     */
    public function testCanCreateIDMLFile(): void
    {
        self::assertFileExists(self::$tempFile);
    }

    /**
     * Tests if the content inside the IDML is equal to the original.
     */
    #[DataProvider('getCanWriteContentData')]
    public function testCanWriteContent(string $fileName, string $contentExpected): void
    {
        $file = self::$tempFolder . DIRECTORY_SEPARATOR . $fileName;
        $files = Helper::scanDirRecursive(self::$tempFolder);

        if (!file_exists($file)) {
            self::fail('File "' . $fileName . '" doesnt exist. The folder contains: ' . var_export($files, true));
        }

        self::assertSame(
            $contentExpected,
            file_get_contents($file)
        );
    }
}
