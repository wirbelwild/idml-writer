<?php

/**
 * Bit&Black IDML Writer. Creating Adobe InDesign Markup Language files.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\IdmlWriter\Tests;

class Helper
{
    /**
     * @return array<int, string>
     */
    public static function scanDirRecursive(string $directory): array
    {
        $result = [];
        $fileNames = scandir($directory);

        if (!is_array($fileNames)) {
            return $result;
        }

        foreach ($fileNames as $fileName) {
            if ('.' === $fileName[0]) {
                continue;
            }

            $filePath = $directory . DIRECTORY_SEPARATOR . $fileName;

            if (is_dir($filePath)) {
                foreach (self::scanDirRecursive($filePath) as $fileNameNested) {
                    $result[] = $fileName . DIRECTORY_SEPARATOR . $fileNameNested;
                }

                continue;
            }

            $result[] = $fileName;
        }

        return $result;
    }
}
