<?php

/**
 * Bit&Black IDML Writer. Creating Adobe InDesign Markup Language files.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\IdmlWriter\Tests;

use BitAndBlack\IdmlWriter\ContentInterface;

/**
 * This class is only for testing purposes. Normally, there should be mostly xml inside those files.
 *
 * @package IDML\Tests
 */
class TempContent implements ContentInterface
{
    /**
     * @return string
     */
    public function getBackingStoryXML(): string
    {
        return 'BACKINGSTORY';
    }

    /**
     * @return string
     */
    public function getContainerXML(): string
    {
        return 'CONTAINER';
    }

    /**
     * @return string
     */
    public function getDesignMapXML(): string
    {
        return 'DESIGNMAP';
    }

    /**
     * @return string
     */
    public function getFontsXML(): string
    {
        return 'FONTS';
    }

    /**
     * @return string
     */
    public function getGraphicXML(): string
    {
        return 'GRAPHIC';
    }

    /**
     * @return array<string, string>
     */
    public function getMasterSpreadXML(): array
    {
        return [
            'SPREAD1' => 'MASTERSPREAD',
        ];
    }

    /**
     * @return string
     */
    public function getMimeType(): string
    {
        return 'MIMETYPE';
    }

    /**
     * @return string
     */
    public function getPreferencesXML(): string
    {
        return 'PREFERENCES';
    }

    /**
     * @return array<string, string>
     */
    public function getSpreadXML(): array
    {
        return [
            'SPREAD1' => 'SPREAD',
        ];
    }

    /**
     * @return array<string, string>
     */
    public function getStoryXML(): array
    {
        return [
            'STORY1' => 'STORY',
        ];
    }

    /**
     * @return string
     */
    public function getStylesXML(): string
    {
        return 'STYLES';
    }

    /**
     * @return string
     */
    public function getTagsXML(): string
    {
        return 'TAGS';
    }

    /**
     * @return string
     */
    public function getMetaDataXML(): string
    {
        return 'METADATA';
    }
}
