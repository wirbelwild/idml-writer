# Changes in IDML Writer v2.0

## 2.0.0 2024-10-11

### Changed

-   PHP >=8.2 is now required.
-   The namespace has been changed from `IDML\Writer` to `BitAndBlack\IdmlWriter`.
-   Dependencies have been updated.