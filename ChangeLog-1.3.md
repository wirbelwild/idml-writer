# Changes in IDML Writer v1.3

## 1.3.0 2021-03-18

### Changed

-   The IDML Writer makes now use of `maennchen/zipstream-php`. This allows to create IDML files without storing files to the file system. 