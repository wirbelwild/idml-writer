[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/idml-writer)](http://www.php.net)
[![Total Downloads](https://poser.pugx.org/bitandblack/idml-writer/downloads)](https://packagist.org/packages/bitandblack/idml-writer)
[![License](https://poser.pugx.org/bitandblack/idml-writer/license)](https://packagist.org/packages/bitandblack/idml-writer)

<p align="center">
    <a href="https://www.bitandblack.com" target="_blank">
        <img src="https://www.bitandblack.com/build/images/BitAndBlack-Logo-Full.png" alt="Bit&Black Logo" width="400">
    </a>
</p>

# Bit&Black IDML Writer

This library writes an Adobe IDML file out of a given content.

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/bitandblack/idml-writer). Add it to your project by running `$ composer require bitandblack/idml-writer`.

## Usage 

When creating a new instance of the Writer, you need to pass your content as first parameter and the IDML tree as second parameter: 

````php
<?php

use BitAndBlack\IdmlWriter\File\Tree;
use BitAndBlack\IdmlWriter\Writer;

$writer = new Writer(
    $myIDMLContent, 
    new Tree()
);
```` 

The content which is `$myIDMLContent` in our example needs to be an object which implements the `ContentInterface`.

Since the structure of an IDML file is always the same, you don't need to customize or set up an own Tree object. But in case you want to you only have to implement the `TreeInterface`.

Create and store the IDML file by running `create()` like that: 

````php
<?php

$writer->create('myFile.idml');
````

Alternatively, if you want to get the content to store it by yourself or do whatever else, call `getContent()`:

````php
<?php

$idmlFileContent = $writer->getContent();

file_put_contents(
    'myFile.idml',
    $idmlFileContent
);
````

## Content of an IDML 

This library doesn't provide creating any IDML content. Creating content can be done by using the `bitandblack/idml-creator` library. Please note that this one has a private license and is not listed public. You can find more information about the IDML Creator library and its documentation under [www.idml.dev/en/idml-creator-php.html](https://www.idml.dev/en/idml-creator-php.html). 

There is also a demo version which you can find under [bitandblack/idml-creator-demo](https://packagist.org/packages/bitandblack/idml-creator-demo).

## Validation of IDML files 

Adobe provides a schema for the validation of IDML files, which can be found under [www.adobe.com](https://www.adobe.com/devnet/indesign/documentation.html). They provide a JAVA-based validation only. 

We as [Bit&Black](https://www.bitandblack.com) made a port to PHP, which can be used and implemented with the help of Composer. It can be found at [packagist.org](https://packagist.org/packages/bitandblack/idml-validator) by its name `bitandblack/idml-validator`.

## Help 

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).