# Changes in IDML Writer

## 1.1.2 2020-10-05

### Fixed

*   Fixed wrong directory separators inside IDML files.

## 1.1.1 2020-10-05

### Fixed

*   Fixed wrong path name that could cause the library to fail when writing a file.

### Added 

*   It's possible now to get a progress bar. Use `enableProgressBar()` and set a Symfony Console Output object there. 

## 1.1.0 2020-02-20 

### Changed 

*   The namespace has been changed from `IDML` to `IDML\Writer`.
*   PHP >= 7.2 is now required.