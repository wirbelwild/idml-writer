# Changes in IDML Writer v2.1

## 2.1.0 2025-03-03

### Changed

-   Improved support for PHP 8.4 by adding missing null types.