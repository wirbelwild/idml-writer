# Changes in IDML Writer

## 1.2.1 2020-11-24

### Added 

-   Added support for PHP 8.

## 1.2.0 2020-10-26

### Changed

-   The ContentInterface requires now the `getMetaDataXML` method. This method allows to add meta data information to an IDML file.